import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LandingComponent } from './modules/landing/components/landing/landing.component';
import { CheckoutComponent } from './modules/landing/components/checkout/checkout.component';
import { InitialComponent } from './modules/landing/components/initial/initial.component';
import { UnderwritingComponent } from './modules/landing/components/underwriting/underwriting.component';
import { SummaryComponent } from './modules/landing/components/summary/summary.component';

const routes: Routes = [
  {
    path: '',
    component: LandingComponent,
    children: [
      {
        path: '',
        component: InitialComponent
      },
      {
        path: 'checkout',
        children: [
          {
            path: '',
            component: CheckoutComponent
          },
          {
            path: 'underwriting',
            component: UnderwritingComponent
          },
          {
            path: 'summary',
            component: SummaryComponent
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
