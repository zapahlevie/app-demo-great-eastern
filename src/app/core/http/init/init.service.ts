import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../api.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InitService {

  constructor(private httpClient: HttpClient) { }

  getLabel(): Observable<any> {
    return this.httpClient[ApiService.getLabel.method]<any>(ApiService.getLabel.url());
  }
}
