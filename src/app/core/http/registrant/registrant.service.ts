import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Registrant } from 'src/app/shared/models/registrant';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ApiService } from '../api.service';

@Injectable({
  providedIn: 'root'
})
export class RegistrantService {

  constructor(private httpClient: HttpClient) { }

  create(registrant): Observable<Registrant> {
    return this.httpClient[ApiService.createRegistrant.method]<Registrant>(ApiService.createRegistrant.url(), JSON.stringify(registrant), ApiService.httpOptions);
  }
  
  getById(id): Observable<Registrant> {
    return this.httpClient[ApiService.getRegistrantById.method]<Registrant>(ApiService.getRegistrantById.url(id));
  }

  getAll(): Observable<Registrant[]> {
    return this.httpClient[ApiService.getAllRegistrants.method]<Registrant[]>(ApiService.getAllRegistrants.url());
  }

  update(id, registrant): Observable<Registrant> {
    return this.httpClient[ApiService.updateRegistrant.method]<Registrant>(ApiService.updateRegistrant.url(id), JSON.stringify(registrant), ApiService.httpOptions);
  }

  delete(id) {
    return this.httpClient[ApiService.deleteRegistrant.method]<Registrant>(ApiService.deleteRegistrant.url(id), ApiService.httpOptions);
  }

  getMultiplier(birtdate): Observable<any> {
    return this.httpClient[ApiService.geMultiplier.method]<any>(ApiService.geMultiplier.url(birtdate));
  }

}
