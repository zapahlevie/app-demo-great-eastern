import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private static apiServer = "http://localhost:9100"

  constructor() { }

  public static httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  public static getLabel = {
    method: 'get',
    url: () => {
      return ApiService.apiServer + '/getLabel';
    }
  }

  public static geMultiplier = {
    method: 'get',
    url: (birtdate) => {
      return ApiService.apiServer + '/multiplier/' + birtdate;
    }
  }

  public static createRegistrant = {
    method: 'post',
    url: () => {
      return ApiService.apiServer + '/registrants/';
    }
  }

  public static getRegistrantById = {
    method: 'get',
    url: (id) => {
      return ApiService.apiServer + '/registrants/' + id;
    }
  }

  public static getAllRegistrants = {
    method: 'get',
    url: () => {
      return ApiService.apiServer + '/registrants/';
    }
  }

  public static updateRegistrant = {
    method: 'put',
    url: (id) => {
      return ApiService.apiServer + '/registrants/' + id;
    }
  }

  public static deleteRegistrant = {
    method: 'delete',
    url: (id) => {
      return ApiService.apiServer + '/registrants/' + id;
    }
  }

}
