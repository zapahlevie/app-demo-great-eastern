export interface Registrant {
    id: number;
    membership: string;
    gender: string;
    birthdate: Date;
    pricingPlan: string;
    multiplier: number;
    price: string;
    fullName: string;
    phone: string;
    email: string;
    birthplace: string;
    identityType: string;
    identityNumber: string;
    addressEqual: boolean;
    identityAddress: string;
    identityProvince: string;
    homeAddress: string;
    homeProvince: string;
    referenceCode: string;
    working: boolean;
    addressForMailing: string;
    bodyHeight: number;
    bodyWeight: number;
    atRisk: boolean;
    benefitTotal: number;
    wakafRate: number;
}
