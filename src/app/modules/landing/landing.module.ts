import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LandingComponent } from './components/landing/landing.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { InitialComponent } from './components/initial/initial.component';
import { UnderwritingComponent } from './components/underwriting/underwriting.component';
import { SummaryComponent } from './components/summary/summary.component';

@NgModule({
  declarations: [
    LandingComponent,
    CheckoutComponent,
    InitialComponent,
    UnderwritingComponent,
    SummaryComponent
  ],
  imports: [
    RouterModule,
    RouterModule,
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class LandingModule { }
