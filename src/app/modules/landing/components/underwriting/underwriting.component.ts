import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { RegistrantService } from 'src/app/core/http/registrant/registrant.service';

@Component({
  selector: 'app-underwriting',
  templateUrl: './underwriting.component.html',
  styleUrls: ['./underwriting.component.css']
})
export class UnderwritingComponent implements OnInit {

  registrantForm: FormGroup;

  constructor(public fb: FormBuilder, private router: Router, public registrantService: RegistrantService) {
    this.registrantForm = this.fb.group({
      membership: [''],
      gender: [''],
      birthdate: [''],
      pricingPlan: [''],
      price: [''],
      multiplier: [''],
      fullName: [''],
      phone: [''],
      email: [''],
      birthplace: [''],
      identityType: [''],
      identityNumber: [''],
      addressEqual: [''],
      identityAddress: [''],
      identityProvince: [''],
      homeAddress: [''],
      homeProvince: [''],
      referenceCode: [''],
      working: [''],
      addressForMailing: [''],
      benefitTotal: [''],
      wakafRate: [''],
      bodyHeight: [''],
      bodyWeight: [''],
      atRisk: ['']
    })
  }

  ngOnInit(): void {
    const val: any = history.state;
    const keys = Object.keys(history.state);
    keys.forEach(key => {
      if (this.registrantForm.controls[key]) {
        this.registrantForm.controls[key].setValue(val[key]);
      }
    });
  }

  submitForm() {

  }

  nextForm() {

    if (this.registrantForm.controls['working'].value === 'true') {
      this.registrantForm.patchValue({ working: true, tc: true });
    }
    else{
      this.registrantForm.patchValue({ working: false, tc: true });
    }

    if (this.registrantForm.controls['atRisk'].value === 'true') {
      this.registrantForm.patchValue({ atRisk: true, tc: true });
    }
    else{
      this.registrantForm.patchValue({ atRisk: false, tc: true });
    }

    this.registrantService.create(this.registrantForm.value).subscribe(res => {
      this.router.navigate(['/checkout/summary'], { state: this.registrantForm.value });
    });

  }

}
