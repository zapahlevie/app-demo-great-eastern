import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { RegistrantService } from 'src/app/core/http/registrant/registrant.service';
import { InitService } from 'src/app/core/http/init/init.service';

@Component({
  selector: 'app-initial',
  templateUrl: './initial.component.html',
  styleUrls: ['./initial.component.css']
})
export class InitialComponent implements OnInit {

  registrantForm: FormGroup;
  label: string;

  constructor(public fb: FormBuilder, private router: Router, public registrantService: RegistrantService, public initService: InitService) {
    this.registrantForm = this.fb.group({
      membership: [''],
      gender: [''],
      birthdate: [''],
      pricingPlan: [''],
      price: [''],
      multiplier: [''],
      fullName: [''],
      phone: [''],
      email: [''],
      birthplace: [''],
      identityType: [''],
      identityNumber: [''],
      addressEqual: [''],
      identityAddress: [''],
      identityProvince: [''],
      homeAddress: [''],
      homeProvince: [''],
      referenceCode: [''],
      working: [''],
      addressForMailing: [''],
      benefitTotal: [''],
      wakafRate: [''],
      bodyHeight: [''],
      bodyWeight: [''],
      atRisk: ['']
    })
  }

  ngOnInit(): void {
    const val: any = history.state;
    const keys = Object.keys(history.state);
    keys.forEach(key => {
      if (this.registrantForm.controls[key]) {
        this.registrantForm.controls[key].setValue(val[key]);
      }
    });
    this.initService.getLabel().subscribe(res => {
      this.label = res.message;
    });
  }

  validateForm(){
    let status = true;
    $('.v-birthdate').html('');
    if(!this.registrantForm.controls['birthdate'].value){
      $('.v-birthdate').html("Mohon masukkan tanggal lahir");
      status = false;
    }
    return status;
  }

  scrollTo(selector) {
    if(this.validateForm()){
      $(selector).removeClass('d-none');
      const topOfElement = window.pageYOffset + selector.getBoundingClientRect().top - 60;
      window.scroll({ top: topOfElement, behavior: "smooth" });
    }
  }

  selectContribution(program) {
    if (program === 'minimal') {
      this.registrantForm.patchValue({ pricingPlan: 'minimal', tc: true });
      this.registrantForm.patchValue({ price: '75000', tc: true });
    }
    else {
      this.registrantForm.patchValue({ pricingPlan: 'maksimal', tc: true });
      this.registrantForm.patchValue({ price: '187500', tc: true });
    }
    this.registrantService.getMultiplier(this.registrantForm.controls['birthdate'].value).subscribe(res => {

      if (res && res.multiplier) {
        let multiplier = res.multiplier;

        this.registrantForm.patchValue({ multiplier: multiplier, tc: true });

        this.router.navigate(['/checkout'], { state: this.registrantForm.value });
      }

      else {
        console.error('error getting data from backend')
      }

    });
  }

}
