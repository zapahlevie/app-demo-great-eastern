import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router, NavigationEnd } from '@angular/router';
import { RegistrantService } from 'src/app/core/http/registrant/registrant.service';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  public registrantForm: FormGroup;

  constructor(public fb: FormBuilder, private router: Router, public registrantService: RegistrantService) {
    this.registrantForm = this.fb.group({
      membership: ['my-self'],
      gender: ['male'],
      birthdate: [''],
      pricingPlan: [''],
      price: [''],
      multiplier: [''],
      fullName: [''],
      phone: [''],
      email: [''],
      birthplace: [''],
      identityType: ['-'],
      identityNumber: [''],
      addressEqual: [true],
      identityAddress: [''],
      identityProvince: [''],
      homeAddress: [''],
      homeProvince: [''],
      referenceCode: [''],
      working: ['true'],
      addressForMailing: ['identity'],
      benefitTotal: [''],
      wakafRate: [''],
      bodyHeight: [''],
      bodyWeight: [''],
      atRisk: ['false']
    })

    this.router.navigate([''], { state: this.registrantForm.value });

  }

  ngOnInit(): void {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0)
    });
  }

  onActivate(componentReference) {
    componentReference.registrantForm.valueChanges.subscribe((data) => {
      this.registrantForm.markAsDirty();
      const keys = Object.keys(data);
      keys.forEach(key => {
        this.registrantForm.controls[key].setValue(data[key]);
      });
    });
  }

}
