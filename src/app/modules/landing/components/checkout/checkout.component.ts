import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RegistrantService } from 'src/app/core/http/registrant/registrant.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

  registrantForm: FormGroup;

  constructor(public fb: FormBuilder, private router: Router) {
    this.registrantForm = this.fb.group({
      membership: [''],
      gender: [''],
      birthdate: [''],
      pricingPlan: [''],
      price: [''],
      multiplier: [''],
      fullName: ['', { Validators: [Validators.required], updateOn: "blur" }],
      phone: [''],
      email: [''],
      birthplace: [''],
      identityType: [''],
      identityNumber: [''],
      addressEqual: [''],
      identityAddress: [''],
      identityProvince: [''],
      homeAddress: [''],
      homeProvince: [''],
      referenceCode: [''],
      working: [''],
      addressForMailing: [''],
      benefitTotal: [''],
      wakafRate: [''],
      bodyHeight: [''],
      bodyWeight: [''],
      atRisk: ['']
    })
  }

  get fullName() {
    return this.registrantForm.get("fullName");
  }

  checkAddressEqual(event) {
    if (event.target.checked) {
      $('.form-home-address').addClass('d-none');
    }
    else {
      $('.form-home-address').removeClass('d-none');
    }
  }

  checkJoinWakaf(event) {
    if (event.target.checked) {
      $('.f-wakafRate').removeClass('d-none');
    }
    else {
      $('.f-wakafRate').addClass('d-none');
    }
  }

  get totalBenefit() {
    return "Rp. " + String(this.registrantForm.get('multiplier').value * this.registrantForm.get('price').value).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
  }

  ngOnInit(): void {
    const val: any = history.state;
    const keys = Object.keys(history.state);
    keys.forEach(key => {
      if (this.registrantForm.controls[key]) {
        this.registrantForm.controls[key].setValue(val[key]);
      }
    });
    if (this.calculateAge(this.registrantForm.controls['birthdate'].value) >= 18) {
      $('.f-joinWakaf').removeClass('d-none');
    }
  }

  calculateAge(birthdate) {
    let timeDiff = Math.abs(Date.now() - new Date(birthdate).getTime());
    let age = Math.floor((timeDiff / (1000 * 3600 * 24)) / 365.25);
    return age;
  }

  validateForm() {
    let status = true;
    $('.v-wakafRate').html('');
    let wakafRate = this.registrantForm.controls['wakafRate'].value;
    if ((wakafRate !== '' && wakafRate !== null) && (wakafRate < 10 || wakafRate > 45)) {
      $('.v-wakafRate').html("Mohon masukkan angka 10 sampai 45");
      status = false;
    }
    return status;
  }

  nextForm() {

    if (this.validateForm()) {

      this.registrantForm.patchValue({ benefitTotal: this.registrantForm.get('multiplier').value * this.registrantForm.get('price').value, tc: true });

      this.router.navigate(['/checkout/underwriting'], { state: this.registrantForm.value });
    }

  }

}
