import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})
export class SummaryComponent implements OnInit {

  membership: string;
  gender: string;
  birthdate: number;
  pricingPlan: string;
  multiplier: number;
  price: string;
  fullName: string;
  phone: string;
  email: string;
  birthplace: string;
  identityType: string;
  identityNumber: string;
  addressEqual: boolean;
  identityAddress: string;
  identityProvince: string;
  homeAddress: string;
  homeProvince: string;
  referenceCode: string;
  working: boolean;
  addressForMailing: string;
  bodyHeight: number;
  bodyWeight: number;
  atRisk: boolean;
  benefitTotal: string;
  wakafRate: number;

  constructor(private router: Router) {

  }

  ngOnInit(): void {
    const val: any = history.state;
    const keys = Object.keys(history.state);
    keys.forEach(key => {
      this[key] = val[key];
    });

    this.benefitTotal = "Rp. " + String(this.benefitTotal).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    this.price = "Rp. " + String(this.price).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    if (this.gender === 'male') {
      this.gender = 'Laki - laki';
    }
    else {
      this.gender = 'Perempuan';
    }

  }

  nextForm() {
    this.router.navigate(['']);
  }

}
